


@page bskSupport Basilisk Support Pages
@tableofcontents




@section chNotes Basilisk Release Notes
- @ref bskReleaseNotes      "Tagged Release Notes"
- @ref bskKnownIssues       "Known Issues Listing"


@section chFAQ  Frequently Asked Questions
- @ref FAQ                  "General Questions"
- @ref FAQmacOS             "macOS Specific Questions"
- [Basilisk Forum](https://hanspeterschaub.info/bskGoogleForum.html)
- @ref useDoxygen           "How to build the Doxygen HTML documentation"
- @ref migratingToPython3   "Migrating BSK Python 2 script to support
- @ref usingLivePlotting    "How do I plot data while a simulation is running rather than plotting after the simulation completes."

@section chCoding Code Development Support
The documentation on how to program custom Basilisk modules is still sparse.  For now the reader is encouraged to study existing Basilisk modules to learn how to program new modules.
- @ref codingGuidelines     "Basilisk Coding Guidelines"
- @ref bskModuleReview      "Module Review Checklist"

@section chBsk Basilisk Architecture Documents
The following list contains documents and resources to help understand the Basilisk simulation architecture.  All these resources are always *work in progress*:
- [Understanding the Basilisk Architecture](UnderstandingBskArchitecture.pptx)


@section chVizard Vizard Support 
- @ref vizard               "Overview of Vizard"
- @ref vizardSettings       "Specifying Vizard Settings from Basilisk"- 
- @ref scenarioBasicOrbitLiveStreamGroup       "Live Streaming from Basilisk to Vizard"