from Basilisk.simulation import gps_module

def test_unit_gpsModule(locations, num_modules):
    g = GPSModule(locations, num_modules)
    g.gps()

if __name__ == '__main__':
    locations = [
        [15600, 7540, 20140, 0.07074],
        [18760, 2750, 18610, 0.0722],
        [17610, 14630, 13480, 0.0769],
        [19170, 610, 18390, 0.07242]
    ]
    test_unit_gpsModule(locations, 4)
