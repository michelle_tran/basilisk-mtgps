#pragma once

#include "_GeneralModuleFiles/sys_model.h"
#include <Eigen/Dense>

class GPSModule: public SysModel
{
private:
  Eigen::MatrixXd positions;
  int num_satellites;
public:
  GPSModule();
  GPSModule(double **satellite_locations, int k);
  Eigen::MatrixXd function_creator(Eigen::MatrixXd guess);
  Eigen::MatrixXd jacobian_creator(Eigen::MatrixXd guess);
  int gps();
};
