#include <iostream>
#include <iomanip>
#include "gps_module.h"

// In kilometers per second
#define LIGHT_SPEED 299792.458

using namespace std;

// Set everything to null
GPSModule::GPSModule()
{
  Eigen::MatrixXd satellite_locations(4,4);
  for(int i = 0; i < 4; i++)
  {
    for(int j = 0; j < 4; j++)
    {
      satellite_locations(i, j) = 0;
    }
  }
  positions = satellite_locations;
  num_satellites = 4;
}

// Set positions to given locations and num_satellites to passed in number
GPSModule::GPSModule(double **satellite_locations, int k)
{
  num_satellites = k;
  for(int i = 0; i < k; i++)
  {
    for(int j = 0; j < 4; j++)
    {
      positions(i, j) = satellite_locations[i][j];
    }
  }
}

Eigen::MatrixXd GPSModule::function_creator(Eigen::MatrixXd guess)
/*
Takes in a matrix of data and a guess and returns a new guess
Guess will always have 4 elements

%The first row of the vector F will always be in the equation since there will
%always be at least 1 satellite.
F=(G(1)-A(1))^2+(G(2)-B(1))^2+(G(3)-C(1))^2-(c*(t(1)-G(4)))^2;

%This iteration adds the approiate row to the vector F.
for i=2:LengthA;
    F= [F;(G(1)-A(i))^2+(G(2)-B(i))^2+(G(3)-C(i))^2-(c*(t(i)-G(4)))^2];
end
ResultF=F;
*/
{
  Eigen::MatrixXd temp(4,num_satellites);
  for(int i = 0; i < num_satellites; i++)
  {
    // TODO: Make sure you figure out whether these matrices are 0 or 1 indexed
    temp(i, 0) = pow(guess(0)-positions(i, 0), 2);
    temp(i, 1) = pow(guess(1)-positions(i, 1), 2);
    temp(i, 2) = pow(guess(2)-positions(i, 2), 2);
    temp(i, 3) = pow(LIGHT_SPEED*(positions(i, 3)-guess(3)), 2);
  }
  return temp;
}

Eigen::MatrixXd GPSModule::jacobian_creator(Eigen::MatrixXd guess)
/*
Takes in a matrix of data and a guess and returns the Jacobian matrix of the new guess
*/
{
  Eigen::MatrixXd temp(4,num_satellites);
  for(int i = 0; i < num_satellites; i++)
  {
    // TODO: Make sure you figure out whether these matrices are 0 or 1 indexed
    temp(i, 0) = 2*(guess(0)-positions(i, 0));
    temp(i, 1) = 2*(guess(1)-positions(i, 1));
    temp(i, 2) = 2*(guess(2)-positions(i, 2));
    temp(i, 3) = 2*(pow(LIGHT_SPEED, 2)*(positions(i, 3)-guess(3)));
  }
  return temp;
}

int GPSModule::gps()
/*
MatrixXd positions: matrix of satellite positions -- x, y, z, t per row (each row = satellite)
int k: the number of satellites
*/
{
  // Setup: make needed constants and turn the 2D array of positions to a matrix
  cout << "Starting GPS calculation" << endl;
  int iterations = 0;
  //double precision = 0.00001;
  Eigen::MatrixXd guess(1, 4);
  // Define guess
  for(int i = 0; i < 4; i++)
  {
    guess(0, i) = 0;
  }

  // Create our function and its Jacobian
  Eigen::MatrixXd func = function_creator(guess);
  Eigen::MatrixXd jaco = jacobian_creator(guess);

  // Multivariate Newton's Method -- TODO: add precision check
  while(iterations < 20)
  {
    Eigen::MatrixXd solution = jaco * -func;
    guess = guess+solution;
    func = function_creator(guess);
    jaco = jacobian_creator(guess);
    iterations++;
  }

  cout << "Final Result: " << guess << endl;

  return 0;
}
